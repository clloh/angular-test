# INTRODUCTION

This is a test project to evaluate how fast a person can learn. More specifically, the language is [Angular (v1)](https://angularjs.org/).

# GOAL

Included is this repository are files that are from a template downloaded from the internet. The coder is required to Angularize the template, more specifically:

1. Using HTTP GET, fetch information from `data.json`. The `data.json` file serves as a mock database response that you'd expect after querying an API endpoint.
2. Display the data, using suitable loops when necessary.
3. When the contact form is submitted, launch a modal that displays a thank you message. The modal design should follow that of `basic modal` in `modal.html`.

# Setup

1. Fork / Clone this repository.
2. Open up index.html in a web browser. As it's purely front-end work, no server is required.
3. Save changes in `js/controller.js`. Create additional files as necessary. Edit any file as necessary.
4. Commit the changes using git and send a merge / pull request.

# Evaluation

The following is considered basic:

- Code works as intended

The following is considered good:

- Code is easy to read
- Code follows consistent styles, e.g. [JavaScript Standard Style](https://standardjs.com/)
- Code is easy to maintain with clear separation of concerns

The following is considered great:

- Great user experience: think about where the user of this website will be confused and prevent that
- Most importantly, think about this project as your own:
    - Any missing data? Think about what is best to do in those situations, e.g. display some message, display default, display nothing, throw error etc. The choice depends on how good a user experience this will be.
    - Anything strange? Think of this as your own business and handle them.
